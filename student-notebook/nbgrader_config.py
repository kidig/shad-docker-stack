import os

DEFAULT_HIDE_GLOBS = '__pycache__ *.pyc *.pyo .DS_Store *.so *.dylib *~ tests nbgrader_config.py'

HIDE_GLOBS = os.getenv('JUPYTER_HIDE_GLOBS', DEFAULT_HIDE_GLOBS).split()


c = get_config()

c.CourseDirectory.root = '/home/jovyan/work'

c.CourseDirectory.db_url = os.getenv('NBGRADER_DATABASE_URL')

c.Exchange.course_id = os.getenv('NBGRADER_COURSE_ID', '')

c.ContentsManager.hide_globs = HIDE_GLOBS