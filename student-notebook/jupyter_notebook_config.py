import os

DEFAULT_HIDE_GLOBS = '__pycache__ *.pyc *.pyo .DS_Store *.so *.dylib *~ tests nbgrader_config.py'

ALLOWED_HOSTS = os.getenv('JUPYTER_ALLOWED_HOSTS', '').split()

HIDE_GLOBS = os.getenv('JUPYTER_HIDE_GLOBS', DEFAULT_HIDE_GLOBS).split()


c = get_config()

c.Exchange.root = '/exchange'

c.NotebookApp.ip = '*'
c.NotebookApp.port = 8888
c.NotebookApp.open_browser = False

c.NotebookApp.tornado_settings = {
    'headers': {
        'Content-Security-Policy':
        "frame-ancestors 'self' {}".format(" ".join(ALLOWED_HOSTS)),
    }
}

c.ContentsManager.hide_globs = HIDE_GLOBS

