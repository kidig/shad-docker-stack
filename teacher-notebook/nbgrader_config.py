import os

NB_USER = os.getenv('NB_USER', 'jovyan')


c = get_config()

c.CourseDirectory.root = '/home/{}/work'.format(NB_USER)

c.CourseDirectory.db_url = os.getenv('NBGRADER_DATABASE_URL')

c.Exchange.course_id = os.getenv('NBGRADER_COURSE_ID', 'default')
